import m5
from m5.objects import *
#SPEC 2006
data_dir = '/home/fangfeil/SPEC2006_bin/data/'
bin_dir = '/home/fangfeil/SPEC2006_bin/alpha/'
#400 perlbench
perlbench = LiveProcess()
perlbench.executable = bin_dir + 'perlbench/perlbench'
lib = '-I'+data_dir + 'perlbench/all/input/lib'
data = data_dir + 'perlbench/ref/input/checkspam.pl'
#output = bin_dir + '/perlbench.ref.checkspam.out'
#errout = bin_dir + '/perlbench.ref.checkspam.err'
perlbench.cmd = [perlbench.executable] + [lib, data, '2500', '5', '25', '11', '150', '1', '1', '1', '1']
#perlbench.output = output
#perlbench.errout = errout

#lbm
lbm = LiveProcess()
lbm.executable = bin_dir + 'lbm/lbm'
data = data_dir + 'lbm/reference.dat'
data2 = data_dir + 'lbm/ref/input/100_100_130_ldc.of'
lbm.cmd = [lbm.executable] + ['3000', data, '0', '0', data2]
#lbm.output = bin_dir + '/lbm/lbm.ref.out'
#lbm.errout = bin_dir + '/lbm/lbm.ref.err'

#libquantum
libquantum = LiveProcess()
libquantum.executable = bin_dir + 'libquantum/libquantum'
libquantum.cmd = [libquantum.executable] + ['1397', '8']
#libquantum.output = bin_dir + '/libquantum/libquantum.ref.out'
#libquantum.errout = bin_dir + '/libquantum/libquantum.ref.err'

#h264ref
h264ref = LiveProcess()
h264ref.executable = bin_dir + 'h264ref/h264ref'
data = data_dir + 'h264ref/ref/input/foreman_ref_encoder_baseline.cfg'
h264ref.cmd = [h264ref.executable] + ['-d', data]
#h264ref.output = bin_dir + '/h264ref/h264ref.ref.foreman_baseline.out'
#h264ref.errout= bin_dir + '/h264ref/h264ref.ref.foreman_baseline.err'

#astar
astar = LiveProcess()
astar.executable = bin_dir + 'astar/astar'
data = data_dir + 'astar/ref/input/BigLakes2048.cfg'
astar.cmd = [astar.executable] + [data]
#astar.output = bin_dir + '/astar/astar.ref.BigLakes2048.out'
#astar.errout = bin_dir + '/astar/astar.ref.BigLakes2048.err'

#milc
milc = LiveProcess()
milc.executable = bin_dir + 'milc/milc'
milc.cmd = [milc.executable]
milc.input = data_dir + 'milc/ref/input/su3imp.in'
#milc.output = bin_dir + '/milc.ref.out'
#milc.errout = bin_dir + '/milc.ref.err'

#mcf
mcf = LiveProcess()
mcf.executable = bin_dir + 'mcf/mcf'
data = data_dir + 'mcf/ref/input/inp.in'
mcf.cmd = [mcf.executable] + [data]
#mcf.output = bin_dir + '/mcf.ref.out'
#mcf.errout = bin_dir + '/mcf.ref.err'

#bzip2
bzip2 = LiveProcess()
bzip2.executable = bin_dir + 'bzip2/bzip2'
data = data_dir + 'bzip2/ref/input/input.source'
bzip2.cmd = [bzip2.executable] + [data, '280']

#gcc
gcc = LiveProcess()
gcc.executable = bin_dir + 'gcc/gcc'
data1 = data_dir + 'gcc/ref/input/c-typeck.i'
data2 = data_dir + 'gcc/ref/output/c-typeck.s'
gcc.cmd = [gcc.executable] + [data1, '-o', data2]

#hmmer
hmmer = LiveProcess()
hmmer.executable = bin_dir + 'hmmer/hmmer'
data1 = data_dir + 'hmmer/ref/input/nph3.hmm'
data2 = data_dir + 'hmmer/ref/input/swiss41'
hmmer.cmd = [hmmer.executable] + [data1, data2]

#sjeng
sjeng = LiveProcess()
sjeng.executable = bin_dir + 'sjeng/sjeng'
data = data_dir + 'sjeng/ref/input/ref.txt'
sjeng.cmd = [sjeng.executable] + [data]

#namd
namd = LiveProcess()
namd.executable = bin_dir + 'namd/namd'
data1 = data_dir + 'namd/all/input/namd.input'
namd.cmd = [namd.executable] + ['--input', data, '--iterations', '1', '--output', 'namd.out']
